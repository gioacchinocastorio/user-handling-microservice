
# UserAuth Microservice #

Distributed Systems final project.

### How do I get set up? ###

Make sure to have installed the following dependencies:
* MongoDB
* Java 1.8
* Gradle (_optional_)

Follow this procedure to set MongoDB up (tested in _Linux_ and _MacOS/Unix_ environments)
1. Install MongoDB [tutorial](https://docs.mongodb.com/v3.4/administration/install-community/)
2. Create the directory `/data/db` in the root folder.
3. Run mongo daemon as root
~~~~
# run sudo mongo —port 27017
~~~~


It's important to add a user to MongoDB using the client.
Use this ref to create a user [tutorial]( https://docs.mongodb.com/v2.6/tutorial/add-user-administrator/).
Important:
~~~~ 
db.createUser(
    ...   {
    ...     user: "myUserAdmin",
    ...     pwd: "abc123",
    ...     roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
    ...   }
    ... )
~~~~
All the information __have to be__ consistent with the configuration file. Change them accordingly to your configuration.
~~~~~
File: configs/config.ini

; Store configuration
[MongoDB]
server_address = localhost
server_port = 27017
db_user_username = myUserAdmin
db_user_password = abc123
...
~~~~~

### Run commands ###

At least in Unix-like environments, it is sufficient to run the MongoDB server
and then to type on terminal the following commands
~~~~~
In the project path

Mac OSX: $ ./gradlew run
Linux: $ sh ./gradlew run
Windows: > gradlew run
~~~~~

Make sure that the listening port is free for the web service. You may change it in the configuration file
~~~~~
File: configs/config.ini

[Spark]
port=12345
~~~~~
