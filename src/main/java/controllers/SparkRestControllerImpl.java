package controllers;

import business.RequestHandler;
import business.pojo.User;
import business.messages.AuthParameters;
import business.messages.AuthRequest;
import business.messages.ErrorMessage;
import org.eclipse.jetty.client.HttpRequest;
import org.eclipse.jetty.http.HttpMethod;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import spark.*;
import utils.config.Configurator;
import utils.customexceptions.AuthException;
import utils.customexceptions.DuplicateUserException;
import utils.requestintegritycheck.IntegrityChecker;
import utils.serialization.Serializer;

import java.util.Objects;

/**
 * JavaSpark REST controller.
 */
public class SparkRestControllerImpl implements Controller {

    /**
     * Configuration information in the config.ini file
     */
    private static final String CONFIGURATION_NAMESPACE = "Spark";
    private static final String PORT_CONFIG = "port";

    /**
     * The web server will listen here
     */
    private int listeningPort = 0;

    /**
     * message format
     */
    private static final String COMMUNICATION_FORMAT = "application/json";

    /**
     * In a route, this is the parameter that contains the userID
     */
    private static final String USER_ID_PARAMETER = ":userid";

    /**
     * Request header for authentication (contains the token)
     */
    private static final String TOKEN_HEADER = "Authorization";

    /**
     * Routes
     */
    private static final String SAME_PATH_ROUTE = "";
    private static final String ROUTE_WILDCARD = "*";

    /**
     * Default response (empty)
     */
    private static final String EMPTY_RESPONSE = "";


    /**
     * Model serializer (object to JSON)
     */
    private Serializer serializer;

    /**
     * Configuration wrapper
     */
    private Configurator configurations;

    /**
     * JSON integrity checker
     */
    private IntegrityChecker integrityChecker;

    /**
     * Business logic handler
     */
    private RequestHandler requestHandler;


    /**
     * Constructor.
     *
     * @param serializer     Model serializer (object to JSON)
     * @param configurations Configuration wrapper
     * @param requestHandler Business logic handler
     * @param checker        JSON integrity checker
     */
    public SparkRestControllerImpl(Serializer serializer,
                                   Configurator configurations,
                                   RequestHandler requestHandler,
                                   IntegrityChecker checker) {
        this.serializer = serializer;
        this.configurations = configurations;
        this.requestHandler = requestHandler;
        this.integrityChecker = checker;

        this.getConfigurations();
    }

    @Override
    public void startListening() {
        this.configureListeningServer();
        this.setupRoutes();
        this.waitServerSetup();

    }

    /**
     * Start the server conf
     */
    private void configureListeningServer() {

        getConfigurations();

        // listening port
        try {
            Spark.port(listeningPort);
        } catch (Exception e) {
            System.err.println("ERROR: the selected port is occupied by another service");
            System.exit(1);
        }

    }

    /**
     * Get the configs from the wrapper
     */
    private void getConfigurations() {
        listeningPort = Integer.parseInt(configurations.getConfiguration(CONFIGURATION_NAMESPACE, PORT_CONFIG));
    }

    /**
     * Activate the routes on the server
     */
    private void setupRoutes() {

        enableCORS(ROUTE_WILDCARD, "GET, POST, PUT, DELETE", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");

        Spark.before(ROUTE_WILDCARD, COMMUNICATION_FORMAT, new LogFilter());

                Spark.path("/api", () -> {

                    Spark.before(ROUTE_WILDCARD, COMMUNICATION_FORMAT, new CheckJSONIntegrityFilter());

                    // user creation
                    Spark.post("/users", COMMUNICATION_FORMAT, new UserCreationRoute(), serializer::serialize);

                    // auth
                    Spark.put("/authorise", COMMUNICATION_FORMAT, new AuthUserRoute(), serializer::serialize);

                    // user handling
                    Spark.path("/user/:userid", () -> {

                        // auth check
                        Spark.before(ROUTE_WILDCARD, COMMUNICATION_FORMAT, new RequestPermissionFilter());

                        // routes
                        Spark.get(SAME_PATH_ROUTE, COMMUNICATION_FORMAT, new UserInfoFetchRoute(), serializer::serialize);
                        Spark.put(SAME_PATH_ROUTE, COMMUNICATION_FORMAT, new UserInfoUpdateRoute());
                        Spark.delete(SAME_PATH_ROUTE, COMMUNICATION_FORMAT, new DeleteUserRoute());

                    });
                }
        );

        // system error handling
        Spark.internalServerError(new InternalErrorRoute()); // server fault
        Spark.notFound(new ResourceNotFoundRoute()); // resource not found

        // set all the responses as JSON
        Spark.after(((request, response) -> response.header("content-type", COMMUNICATION_FORMAT)));


    }


    /**
     * Wait for the server to set up
     */
    private void waitServerSetup() {

        // this blocks the thread
        Spark.awaitInitialization(); // Wait for server to be initialized

    }

    /**
     * Creation of response for badly structured requests
     *
     * @param response http response wrapper
     * @return error message
     */
    private ErrorMessage setMalformedRequest(Response response) {
        ErrorMessage responseContent;
        response.status(HttpStatus.UNPROCESSABLE_ENTITY_422);
        responseContent = ErrorMessage.getMalformedRequest();
        return responseContent;
    }

    /**
     * Create the user route
     */
    private class UserCreationRoute implements Route {

        @Override
        public Object handle(Request request, Response response) throws Exception {

            Object responseContent = null;
            String jsonUser = request.body();
            User user;

            if (integrityChecker.isUserWellFormed(jsonUser)) {

                // retrieve user from the json serialization
                user = (User) serializer.deserialize(jsonUser, User.class);

                try {

                    // create new user
                    requestHandler.createNewUser(user);

                    responseContent = EMPTY_RESPONSE;

                    // successfull creation
                    response.status(HttpStatus.CREATED_201);

                } catch (DuplicateUserException exception) { // duplicate index exception!!!

                    response.status(HttpStatus.BAD_REQUEST_400);
                    responseContent = ErrorMessage.getDuplicateUser();


                }

            } else {
                responseContent = setMalformedRequest(response);
            }


            return responseContent;
        }


    }


    /**
     * User auth route
     */
    private class AuthUserRoute implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            Object responseContent;
            String jsonRequest = request.body();
            AuthRequest authRequest;

            if (integrityChecker.isLoginRequestWellFormed(jsonRequest)) {
                authRequest = (AuthRequest) serializer.deserialize(jsonRequest, AuthRequest.class);
                try {
                    responseContent = requestHandler.authoriseUser(authRequest);

                } catch (AuthException e) {
                    response.status(HttpStatus.NOT_FOUND_404);
                    responseContent = ErrorMessage.getNoSuchUser();
                }
            } else {
                responseContent = setMalformedRequest(response);
            }


            return responseContent;
        }
    }

    /**
     * Check if the user is authorized
     */
    private class RequestPermissionFilter implements Filter {

        @Override
        public void handle(Request request, Response response) throws HaltException {

            String userid = request.params(USER_ID_PARAMETER);
            String authToken = request.headers(TOKEN_HEADER);

            // avoid pre-flight checks
            if (!Objects.equals(request.requestMethod(), "OPTIONS")) {

                AuthParameters authParameters = new AuthParameters(authToken, userid);

                if (!requestHandler.isRequestAuthorised(authParameters)) {
                    Spark.halt(HttpStatus.UNAUTHORIZED_401);
                }
            }


        }
    }

    /**
     * Retrieve user info route
     */
    private class UserInfoFetchRoute implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            // retrieve user id from the request
            String userid = request.params(USER_ID_PARAMETER);

            // 200 is default for get

            // because of auth, the user can be always retrieved
            return requestHandler.getUser(userid);
        }
    }

    /**
     * Update user route
     */
    private class UserInfoUpdateRoute implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            // retrieve user id from the request
            String userid = request.params(USER_ID_PARAMETER);
            String jsonUserNewInfo = request.body();
            Object responseContent;

            if (integrityChecker.isUserWellFormed(jsonUserNewInfo)) {
                User updateInfos = (User) serializer.deserialize(jsonUserNewInfo, User.class);

                updateInfos.setUserID(userid);

                try {

                    requestHandler.updateUser(updateInfos);
                    response.status(HttpStatus.NO_CONTENT_204);
                    responseContent = EMPTY_RESPONSE;

                } catch (DuplicateUserException e) {
                    response.status(HttpStatus.BAD_REQUEST_400);
                    responseContent = ErrorMessage.getDuplicateUser();
                }
            } else {
                responseContent = setMalformedRequest(response);
            }

            return responseContent;
        }
    }

    /**
     * Delete user route
     */
    private class DeleteUserRoute implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            // retrieve user id from the request
            String userid = request.params(USER_ID_PARAMETER);

            requestHandler.deleteUser(userid);

            response.status(HttpStatus.NO_CONTENT_204);

            return EMPTY_RESPONSE;
        }
    }

    /**
     * Route for 500 error
     */
    private class InternalErrorRoute implements Route {

        @Override
        public Object handle(Request request, Response response) throws Exception {
            return serializer.serialize(ErrorMessage.getInternalError());
        }
    }

    /**
     * Route for 404 error
     */
    private class ResourceNotFoundRoute implements Route {

        @Override
        public Object handle(Request request, Response response) throws Exception {
            return serializer.serialize(ErrorMessage.getNotFoundError());
        }
    }

    /**
     * Checks if the request is is proper JSON
     */
    private class CheckJSONIntegrityFilter implements Filter {

        @Override
        public void handle(Request request, Response response) throws Exception {

            // if there are data to put in the server
            if (Objects.equals(request.requestMethod(), HttpMethod.POST.asString()) || Objects.equals(request.requestMethod(), HttpMethod.PUT.asString())) {
                String json = request.body();

                // check if the request is valid json
                try {
                    new JSONObject(json);
                } catch (JSONException e) {
                    Spark.halt(HttpStatus.BAD_REQUEST_400);
                }
            }
        }
    }

    // Enables CORS on requests. This method is an initialization method and should be called once.
    // https://sparktutorials.github.io/2016/05/01/cors.html
    private static void enableCORS(final String origin, final String methods, final String headers) {

        Spark.options("/*", (request, response) -> {


            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            String accessControlRequestCredential = request.headers("Access-Control-Allow-Credentials");
            if (accessControlRequestCredential != null) {
                response.header("Access-Control-Allow-Credentials", "true");
            }

            return "OK";
        });

        Spark.before((request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
            response.header("Access-Control-Request-Method", methods);
            response.header("Access-Control-Allow-Headers", headers);
            response.header("Access-Control-Allow-Credentials", "true");
            // Note: this may or may not be necessary in your particular application
            response.type("application/json");
        });
    }

    /**
     * Log the incoming requests
     */
    private class LogFilter implements Filter {
        @Override
        public void handle(Request request, Response response) throws Exception {

            System.out.println("Received request");
            System.out.println("Method: " + request.requestMethod());
            System.out.println("Client address: " + request.ip());
            System.out.println("Route: " + request.pathInfo());
            System.out.println("Content: " + request.body());
            System.out.println("\n\n");


        }
    }
}
