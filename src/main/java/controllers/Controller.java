package controllers;

/**
 * Interface that specifies how a controller should be started
 */
public interface Controller {

    /**
     * It will start the controller
     */
    void startListening();


}
