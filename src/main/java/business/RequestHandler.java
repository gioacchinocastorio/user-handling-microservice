package business;

import business.messages.AuthParameters;
import business.messages.AuthRequest;
import business.messages.AuthResponse;
import business.pojo.User;
import utils.customexceptions.AuthException;
import utils.customexceptions.DuplicateUserException;
import utils.customexceptions.NoUserException;

/**
 * This entity hides the business logic of the system.
 */
public interface RequestHandler {

    /**
     * User creation procedure
     *
     * @param newUser user to store
     * @throws Exception Thrown if the specified user is already in the system (same email)
     */
    void createNewUser(User newUser) throws Exception;

    /**
     * User update procedure
     *
     * @param userInfo new user info
     * @throws DuplicateUserException Thrown if the user has an email already specified in the system
     */
    void updateUser(User userInfo) throws DuplicateUserException;

    /**
     * Remove a user from the store
     *
     * @param userId id of the user
     */
    void deleteUser(String userId);

    /**
     * Give an authorisation to the user (token)
     *
     * @param authRequest request
     * @return token weapper
     * @throws AuthException Thrown the user is not authorized to have a token
     */
    AuthResponse authoriseUser(AuthRequest authRequest) throws AuthException;

    /**
     * User retrieval procedure
     * @param userID ID of the user in the system
     * @return requested user
     * @throws NoUserException Thrown if the specified user is not in the system
     */
    User getUser(String userID) throws NoUserException;

    /**
     * Check if the request is authorized according to this server
     *
     * @param parameters Authentication parameters object
     * @return true if the request is authorized
     */
    boolean isRequestAuthorised(AuthParameters parameters);


}
