package business;

import business.messages.AuthParameters;
import business.messages.AuthResponse;
import business.messages.AuthRequest;
import business.pojo.User;
import utils.customexceptions.AuthException;
import utils.customexceptions.DuplicateUserException;
import utils.customexceptions.NoUserException;
import utils.digest.DigestStrategy;
import utils.storage.UserStorageDAO;
import utils.token.TokenStrategy;

import java.util.Objects;

/**
 * Basic implementation fo the RequestHandler interface.
 * See the interface to get more details
 */
public class RequestHandlerImpl implements RequestHandler {

    /**
     * User store point-of-access
     */
    private final UserStorageDAO storageDAO;

    /**
     * Hashing procedure
     */
    private final DigestStrategy digestStrategy;

    /**
     * Auth token handler
     */
    private final TokenStrategy tokenStrategy;

    /**
     * Constructor.
     *
     * @param storageDAO User store point-of-access
     * @param hasher     Hashing procedure
     * @param tokenGen   Auth token handler
     */
    public RequestHandlerImpl(UserStorageDAO storageDAO,
                              DigestStrategy hasher,
                              TokenStrategy tokenGen) {
        this.storageDAO = storageDAO;
        this.digestStrategy = hasher;
        tokenStrategy = tokenGen;
    }

    @Override
    public void createNewUser(User newUser) throws Exception {

        // hash incoming password
        this.hashPassword(newUser);

        // semplified way to activate the user
        newUser.setActive(true);

        storageDAO.storeUser(newUser);

    }

    @Override
    public void updateUser(User userInfo) throws DuplicateUserException {

        // hash incoming password
        this.hashPassword(userInfo);

        // retrieve the old infos
        User storeduser = storageDAO.fetchUserByID(userInfo.getUserID());

        storeduser.update(userInfo);

        storageDAO.storeUser(storeduser);


    }

    @Override
    public void deleteUser(String userId) {

        // retrieve the infos
        User storeduser = storageDAO.fetchUserByID(userId);

        // the user is no longer active and won't be served
        storeduser.setActive(false);

        try {
            storageDAO.storeUser(storeduser);
        } catch (DuplicateUserException e) {
            // it cannot happen
            System.err.println("ERROR: internal logic error, stored user with different email");
            System.exit(1);
        }


    }

    @Override
    public boolean isRequestAuthorised(AuthParameters parameters) {

        return this.tokenStrategy.isTokenValid(parameters.getAuthToken(), parameters.getUserId());

    }

    @Override
    public AuthResponse authoriseUser(AuthRequest authRequest) throws AuthException {

        User retrieveduser;
        String hashedpassword;
        String authToken;

        // hash received password
        hashedpassword = digestStrategy.hash(authRequest.getPassword());

        retrieveduser = storageDAO.fetchUserByEmail(authRequest.getEmail());


        // the user has to be existent, with matching password and active
        if (retrieveduser == null ||
                !Objects.equals(retrieveduser.getPassword(), hashedpassword) ||
                !retrieveduser.isActive()) {
            throw new AuthException();
        }

        // retrieve the token from the user
        authToken = retrieveduser.getAuthToken();

        if (authToken != null && this.tokenStrategy.isTokenExpired(authToken)) {
            // not expired
            // give the old token
            authToken = retrieveduser.getAuthToken();

        } else {
            // generate a new token
            authToken = this.tokenStrategy.generateAuthToken(retrieveduser);
            retrieveduser.setAuthToken(authToken);

            // store the token
            try {
                storageDAO.storeUser(retrieveduser);
            } catch (DuplicateUserException e) {
                System.err.println("ERROR: internal logic error, stored user with different email");
                System.exit(1);
            }
        }

        return new AuthResponse(authToken, retrieveduser.getUserID());


    }

    @Override
    public User getUser(String userID) throws NoUserException {

        User retrieved = storageDAO.fetchUserByID(userID);

        if (retrieved == null) {
            throw new NoUserException();
        }

        return retrieved;
    }

    /**
     * Generate the hash of the password
     * @param user new user to store. This object contains the password
     */
    private void hashPassword(User user) {

        String password = user.getPassword();
        password = digestStrategy.hash(password);
        user.setPassword(password);

    }

}
