package business.pojo;

import com.google.gson.annotations.Expose;

/**
 * Representation the user in the system
 */
public class User {

    @Expose private String userID;

    @Expose private String email;
    @Expose(serialize = false) private String password;

    @Expose private String firstName;
    @Expose private String lastName;

    /**
     * Birth date in millis from Unix Epoch
     */
    @Expose private long birthdate;

    @Expose private String gender;

    private boolean active;
    private String authToken;



    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(long birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Update user infos
     * @param updateData container of the new infos
     */
    public void update(User updateData) {

        this.firstName = updateData.firstName;
        this.lastName = updateData.lastName;
        this.lastName = updateData.lastName;
        this.gender = updateData.gender;
        this.password = updateData.password;
        this.birthdate = updateData.birthdate;
        this.email = updateData.email;

    }

    @Override
    public String toString() {
        return "User{" +
                "userID='" + userID + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthdate=" + birthdate +
                ", gender='" + gender + '\'' +
                ", authToken='" + authToken + '\'' +
                '}';
    }
}
