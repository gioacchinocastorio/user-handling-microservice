package business.messages;

/**
 * This class represents the authorization parameters from a client.
 * It encapsulates the auth token and the id of the user who issued the request.
 */
public class AuthParameters {

    /**
     * Authorization token JWT as string, given by the User Micro Service
     */
    private String authToken;

    /**
     * Identifier of the user in the system, given by the User Micro Service
     */
    private String userId;

    public AuthParameters(String authToken, String userId) {
        this.authToken = authToken;
        this.userId = userId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public String getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "AuthParameters{" +
                "authToken='" + authToken + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
