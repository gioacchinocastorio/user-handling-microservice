package business.messages;

import com.google.gson.annotations.Expose;

/**
 * This class represents the authorization request from a client.
 * It encapsulates the auth token and the id of the user who issued the request.
 */
public class AuthRequest {

    @Expose private String email;
    @Expose private String password;


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "AuthRequest{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
