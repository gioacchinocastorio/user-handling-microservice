package business.messages;

import com.google.gson.annotations.Expose;

/**
 * This is an utility factory class.  It doesn't need internationalization.
 * It encapsulates all the error responses.
 */
public class ErrorMessage {

    /**
     * error responses
     */
    private static final String DUPLICATED_USER_ERROR = "There is an existing user with this email";
    private static final String MALFORMED_REQUEST = "Badly structured request";
    private static final String NO_SUCH_USER = "No user with these specifications";
    private static final String INTERNAL_SERVER_ERROR = "Something went wrong with the server";
    private static final String NOT_EXISTING_RESOURCE = "Sorry, not found!";

    /**
     * Issued when there is a creation attempt of a duplicate user
     *
     * @return Duplicate user error
     */
    public static ErrorMessage getDuplicateUser() {

        return new ErrorMessage(DUPLICATED_USER_ERROR);
    }

    /**
     * Issued when the specified request does not conform to the API spec
     *
     * @return Malformed Request Error
     */
    public static ErrorMessage getMalformedRequest() {

        return new ErrorMessage(MALFORMED_REQUEST);
    }

    /**
     * Issued when the specified user is not in the system
     *
     * @return No such user error
     */
    public static ErrorMessage getNoSuchUser() {

        return new ErrorMessage(NO_SUCH_USER);
    }

    /**
     * Issued when the server crashes
     *
     * @return Internal (generic) error
     */
    public static ErrorMessage getInternalError() {
        return new ErrorMessage(INTERNAL_SERVER_ERROR);
    }

    public static ErrorMessage getNotFoundError() {
        return new ErrorMessage(NOT_EXISTING_RESOURCE);
    }


    @Expose
    private String error;

    private ErrorMessage(String err) {
        this.error = err;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
