package business.messages;

import com.google.gson.annotations.Expose;

/**
 * This class represents the authorization response of the server
 */
public class AuthResponse {

    /**
     * Authorization token JWT as string, given by the User Micro Service
     */
    @Expose private String authToken;

    /**
     * Identifier of the user in the system, given by the User Micro Service
     */
    @Expose private String userId;

    public AuthResponse(String authToken, String userId) {
        this.authToken = authToken;
        this.userId = userId;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "AuthResponse{" +
                "authToken='" + authToken + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
