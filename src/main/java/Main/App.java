package Main;

import business.RequestHandler;
import business.RequestHandlerImpl;
import controllers.Controller;
import controllers.SparkRestControllerImpl;
import utils.config.Configurator;
import utils.config.ConfiguratorIniImpl;
import utils.digest.DigestStrategy;
import utils.digest.MD5DigestStrategyImpl;
import utils.requestintegritycheck.IntegrityChecker;
import utils.requestintegritycheck.JSONSchemaDraft6IntegrityCheckerImpl;
import utils.serialization.GsonSerializerImpl;
import utils.serialization.Serializer;
import utils.storage.UserStorageDAO;
import utils.storage.morphia.MorphiaUserStorageDAOImpl;
import utils.token.JWTTokenStrategyImpl;
import utils.token.TokenStrategy;

import java.io.File;

/**
 * Application point of access
 */
public class App {

    public static void main(String[] args) {

        // configurations
        File ConfigFile = new File("configs/config.ini");
        Configurator configurator = new ConfiguratorIniImpl(ConfigFile);

        // utils
        Serializer serializer = new GsonSerializerImpl();
        DigestStrategy hasher = new MD5DigestStrategyImpl();
        TokenStrategy tokengen = new JWTTokenStrategyImpl(configurator);
        IntegrityChecker checker = new JSONSchemaDraft6IntegrityCheckerImpl(configurator);

        // store
        UserStorageDAO storageDAO = new MorphiaUserStorageDAOImpl(configurator);

        // business logic
        RequestHandler requestHandler = new RequestHandlerImpl(storageDAO, hasher, tokengen);

        // start the web server
        Controller userService = new SparkRestControllerImpl(serializer, configurator, requestHandler, checker);
        userService.startListening();


    }

}
