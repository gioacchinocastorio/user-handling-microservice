package utils.requestintegritycheck;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import utils.config.Configurator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.Field;

/**
 * JSONSchemaDraft6 implementation of the integrity checker
 */
public class JSONSchemaDraft6IntegrityCheckerImpl implements IntegrityChecker {

    /**
     * Configuration specs
     */
    private static final String CONFIGURATION_NAMESPACE = "JSONSchemaChecker";
    private static final String USER_SCHEMA_PATH = "user_schema_path";
    private static final String AUTH_SCHEMA_PATH = "auth_schema_path";

    /**
     * Checker schemas (they wrap the JSON Schema Draft 6 spec)
     */
    private Schema userCheker;
    private Schema authChecker;

    /**
     * Constructor
     *
     * @param configurator configuration wrapper
     */
    public JSONSchemaDraft6IntegrityCheckerImpl(Configurator configurator) {

        this.configure(configurator);

    }

    /**
     * Retrieve the configurations
     *
     * @param configurator configuration wrapper
     */
    private void configure(Configurator configurator) {

        String userSchemaPath = configurator.getConfiguration(CONFIGURATION_NAMESPACE, USER_SCHEMA_PATH);
        String authSchemaPath = configurator.getConfiguration(CONFIGURATION_NAMESPACE, AUTH_SCHEMA_PATH);

        this.userCheker = this.retrieveSchema(userSchemaPath);
        this.authChecker = this.retrieveSchema(authSchemaPath);


    }

    @Override
    public boolean isUserWellFormed(String userSerialized) {

        return this.checkBySchema(userSerialized, this.userCheker);

    }

    @Override
    public boolean isLoginRequestWellFormed(String loginSerialized) {

        return this.checkBySchema(loginSerialized, this.authChecker);

    }


    /**
     * Retrieve the schema from the spec JSON file
     *
     * @param schemaPath path of the spec JSON file
     * @return retrieved schema
     */
    private Schema retrieveSchema(String schemaPath) {

        File schemaFile = new File(schemaPath);
        Schema retrievedSchema = null;

        try {

            InputStream targetStream = new FileInputStream(schemaFile);
            JSONObject rawSchema = new JSONObject(new JSONTokener(targetStream));
            retrievedSchema = SchemaLoader.load(rawSchema);

        } catch (FileNotFoundException e) {
            System.err.println("ERROR: unable to retrieve the JSON check schema " + schemaPath );
            System.exit(1);
        }

        return retrievedSchema;


    }

    /**
     * Check if the specified json serialization complies with specified schema
     *
     * @param serialized json serialized object
     * @param checker    json schema
     * @return true if the validation is successful
     */
    private boolean checkBySchema(String serialized, Schema checker) {

        boolean wellformed = true;

        try {
            checker.validate(new JSONObject(serialized));

        } catch (ValidationException | JSONException e) {
            wellformed = false;
        }

        return wellformed;
    }
}
