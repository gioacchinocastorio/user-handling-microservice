package utils.requestintegritycheck;

/**
 * Class to check if the requests are well formed
 */
public interface IntegrityChecker {

    /**
     * Check if the new user request is well formed
     *
     * @param userSerialized User to create
     * @return true if well formed
     */
    boolean isUserWellFormed(String userSerialized);

    /**
     * Check if the login request is well formed
     *
     * @param loginSerialized login request
     * @return true if well formed
     */
    boolean isLoginRequestWellFormed(String loginSerialized);

}
