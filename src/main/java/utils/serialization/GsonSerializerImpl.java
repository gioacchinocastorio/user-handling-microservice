package utils.serialization;

import com.google.gson.*;
import org.bson.types.ObjectId;
import org.json.JSONObject;

import java.lang.reflect.Type;

/**
 * This class is just an adapter for Gson
 */
public class GsonSerializerImpl implements Serializer {

    /**
     * Actual serializer
     */
    private final Gson marshaller;

    public GsonSerializerImpl() {
        // this handles MongoDB ObjectIDs
        marshaller = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .setPrettyPrinting()
                .create();
    }

    @Override
    public String serialize(Object object) {
        return marshaller.toJson(object);
    }

    @Override
    public Object deserialize(String serializedString, Type prototype) {
        return marshaller.fromJson(new JSONObject(serializedString).toString(), prototype);
    }

}
