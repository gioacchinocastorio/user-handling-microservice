package utils.storage.morphia;

import business.pojo.User;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import utils.storage.UserWrapper;

/**
 * Morphia implementation for the user adapter
 */
@Entity(value = "users", noClassnameStored = true)
public class UserMorphiaAdapter implements UserWrapper {

    @Id
    private ObjectId userID;

    @Indexed(unique = true)
    private String email;

    private String password;
    private String firstName;
    private String lastName;
    private long birthdate;
    private String gender;
    private String token;
    private boolean active;

    public UserMorphiaAdapter() {
    }

    public UserMorphiaAdapter(User userToWrap) {

        // retrieve data from the user messages
        this.setUserID(userToWrap.getUserID());
        email = userToWrap.getEmail();
        password = userToWrap.getPassword();
        firstName = userToWrap.getFirstName();
        lastName = userToWrap.getLastName();
        birthdate = userToWrap.getBirthdate();
        gender = userToWrap.getGender();
        token = userToWrap.getAuthToken();
        active = userToWrap.isActive();

    }

    private String getUserID() {
        return userID.toString();
    }

    private void setUserID(String userID) {
        if (userID != null) {
            this.userID = new ObjectId(userID);
        }
    }

    @Override
    public User getWrappedUser() {

        User wrappedUser = new User();

        wrappedUser.setUserID(this.getUserID());
        wrappedUser.setEmail(email);
        wrappedUser.setPassword(password);
        wrappedUser.setFirstName(firstName);
        wrappedUser.setLastName(lastName);
        wrappedUser.setGender(gender);
        wrappedUser.setBirthdate(birthdate);
        wrappedUser.setAuthToken(token);
        wrappedUser.setActive(active);

        return wrappedUser;
    }

}
