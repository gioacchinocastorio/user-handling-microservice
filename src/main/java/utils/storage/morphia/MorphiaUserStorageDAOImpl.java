package utils.storage.morphia;

import business.pojo.User;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import utils.config.Configurator;
import utils.customexceptions.DuplicateUserException;
import utils.storage.UserStorageDAO;

import java.util.Arrays;

/**
 * Morphia storage DAO.
 * See the interface for more info
 */
public class MorphiaUserStorageDAOImpl implements UserStorageDAO {

    private static final String CONFIGURATION_NAMESPACE = "MongoDB";

    private static final String SERVER_ADDRESS_CONFIG = "server_address";
    private static final String SERVER_PORT_CONFIG = "server_port";

    private static final String DB_USERNAME = "db_user_username";
    private static final String DB_PASSWORD = "db_user_password";
    private static final String DB_CREDENTIAL_STORE = "credential_store_name";

    private static final String SERVICE_USER_STORE_NAME = "user_store_name";
    private static final String EMAIL_FIELD = "email";

    /**
     * Proxy of the server
     */
    private Datastore datastore;

    /**
     * MongoDB server infos
     */
    private String mongodbAddress;
    private int mongoPort;

    /**
     * Name of the user stores
     */
    private String storeName;


    /**
     * User auth informations
     */
    private String storeUserName;
    private char[] userPassword;
    private String userStoreName;


    /**
     * DB client with configurations
     * @param configurator Configuration wrapper
     */
    public MorphiaUserStorageDAOImpl(Configurator configurator) {

        this.retrieveConfigurations(configurator);

        this.setupClient();
    }

    /**
     * Setup of the morphia client
     */
    private void setupClient() {
        try {
            Morphia storageConfig = new Morphia();
            storageConfig.mapPackageFromClass(UserMorphiaAdapter.class);


            MongoCredential credential = MongoCredential.createCredential(storeUserName, userStoreName, userPassword);
            MongoClient client = new MongoClient(new ServerAddress(mongodbAddress, mongoPort), Arrays.asList(credential));

            datastore = storageConfig.createDatastore(client, storeName);
            datastore.ensureIndexes();
        } catch (Exception e) {
            System.err.println("ERROR: it seems that the store is not available or you're not authorized to access its data");
            System.exit(1);
        }
    }

    /**
     * Retrieve infos from the configuration file
     * @param configurator configuration wrapper
     */
    private void retrieveConfigurations(Configurator configurator) {

        // server address infos
        mongodbAddress = configurator.getConfiguration(CONFIGURATION_NAMESPACE, SERVER_ADDRESS_CONFIG);
        mongoPort = Integer.parseInt(configurator.getConfiguration(CONFIGURATION_NAMESPACE, SERVER_PORT_CONFIG));

        // access credential
        storeUserName = configurator.getConfiguration(CONFIGURATION_NAMESPACE, DB_USERNAME);
        userPassword = configurator.getConfiguration(CONFIGURATION_NAMESPACE, DB_PASSWORD).toCharArray();
        userStoreName = configurator.getConfiguration(CONFIGURATION_NAMESPACE, DB_CREDENTIAL_STORE);

        // user store infos
        storeName = configurator.getConfiguration(CONFIGURATION_NAMESPACE, SERVICE_USER_STORE_NAME);

    }

    @Override
    public User fetchUserByEmail(String email) {

        UserMorphiaAdapter adapter;
        User retrieved = null;

        adapter = datastore.createQuery(UserMorphiaAdapter.class)
                .field(EMAIL_FIELD).equal(email).get();

        if (adapter != null) {
            retrieved = adapter.getWrappedUser();
        }


        // the first user of the list
        return retrieved;
    }

    @Override
    public User fetchUserByID(String userId) {

        UserMorphiaAdapter retrievedUser = datastore.get(UserMorphiaAdapter.class, new ObjectId(userId));
        return retrievedUser.getWrappedUser();

    }

    @Override
    public void storeUser(User userToSave) throws DuplicateUserException {

        try {
            datastore.save(new UserMorphiaAdapter(userToSave));
        } catch (DuplicateKeyException e) {
            // if the user is already in the store
            throw new DuplicateUserException();
        }


    }
}
