package utils.storage;

import business.pojo.User;

/**
 * Storage adapter interface for the user
 */
public interface UserWrapper {

    /**
     * Return the wrapped user
     * @return wrapped user messages
     */
    User getWrappedUser();
}
