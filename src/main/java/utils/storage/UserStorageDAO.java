package utils.storage;

import business.pojo.User;
import utils.customexceptions.DuplicateUserException;

/**
 * Point of Access to the the underlying storage
 */
public interface UserStorageDAO {

    /**
     * Retrieve user from the storage
     * @param email email of the user
     * @return user infos
     */
    User fetchUserByEmail(String email);

    /**
     * Retrieve user from the storage
     * @param userId identifier of the user
     * @return user infos
     */
    User fetchUserByID(String userId);

    /**
     * Save the user infos in the storage. If it does not exists, it creates one
     * @param userToSave information to save
     * @exception DuplicateUserException if a use with the specified email is in the store
     */
    void storeUser(User userToSave) throws DuplicateUserException;

}
