package utils.digest;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * MD5 implementation of the digest generation
 */
public class MD5DigestStrategyImpl implements DigestStrategy {

    /**
     * Hashing method
     */
    private static final String METHOD = "MD5";

    /**
     * Actual digester
     */
    private MessageDigest hasher;

    public MD5DigestStrategyImpl() {

        try {
            hasher = MessageDigest.getInstance(METHOD);
        } catch (NoSuchAlgorithmException e) {
            System.err.println("ERROR: inpossible to retrieve the " + METHOD + " hasher");
            System.exit(1);
        }

    }

    @Override
    public String hash(String tohash) {
        byte[] hashedbytes = this.hasher.digest(tohash.getBytes());
        return new String(hashedbytes);
    }
}
