package utils.digest;

/**
 * Hash generator
 */
public interface DigestStrategy {

    /**
     * Generate an hash of the specified string
     * @param tohash string to hash
     * @return unique hashed string
     */
    String hash(String tohash);

}
