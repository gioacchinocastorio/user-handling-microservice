package utils.token;

import business.pojo.User;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import utils.config.Configurator;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;

/**
 * Token JWT implementation.
 */
public class JWTTokenStrategyImpl implements TokenStrategy {

    /**
     * Config specs
     */
    private static final String CONFIGURATION_NAMESPACE = "JWT";
    private static final String SECURE_KEY = "key";
    private static final String ISSUER_NAME = "issuer";
    private static final String EXPIRATION_DAY_LENGTH = "days_to_expiration";

    /**
     * Symmetric HMAC256 encryption key
     */
    private String encryptionKey;

    /**
     * Name of the token issue authority
     */
    private String issuerName;

    /**
     * Ref to the token gen algorithm
     */
    private Algorithm algorithm;

    /**
     * Token expiration duration
     */
    private int expirationDuration;


    @Override
    public String generateAuthToken(User user) {

        Calendar calendar = Calendar.getInstance();
        Date exipringdate = new Date();

        calendar.setTime(exipringdate);
        calendar.add(Calendar.DATE, expirationDuration); // ad a day to the token expiring date
        exipringdate = calendar.getTime();

        return JWT.create()
                .withIssuer(issuerName)
                .withSubject(user.getUserID())
                .withExpiresAt(exipringdate)
                .sign(algorithm);
    }

    @Override
    public boolean isTokenExpired(String token) {

        boolean expired = false;

        DecodedJWT jwt;

        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer(issuerName)
                .build(); //Reusable verifier instance

        try {
            jwt = verifier.verify(token);
        } catch (TokenExpiredException e) {
            expired = true;
        }

        return expired;
    }

    @Override
    public boolean isTokenValid(String token, String userId) {

        boolean valid = true;

        if (token != null) {
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(issuerName)
                    .withSubject(userId)
                    .build(); //Reusable verifier instance

            try {
                verifier.verify(token);

            } catch (JWTVerificationException e) {
                valid = false;
            }
        }
        else {
            valid = false;
        }


        return valid;
    }

    /**
     * Constructor
     *
     * @param configurator configuration wrapper
     */
    public JWTTokenStrategyImpl(Configurator configurator) {

        this.configure(configurator);

        this.setupAlgorithm();


    }

    private void setupAlgorithm() {
        try {
            algorithm = Algorithm.HMAC256(encryptionKey);
        } catch (UnsupportedEncodingException e) {
            System.err.println("ERROR: Unable to retrieve the configurations!");
            System.exit(1);
        }
    }

    /**
     * Retrieve the configurations
     *
     * @param configurator configuration wrapper
     */
    private void configure(Configurator configurator) {

        encryptionKey = configurator.getConfiguration(CONFIGURATION_NAMESPACE, SECURE_KEY);
        issuerName = configurator.getConfiguration(CONFIGURATION_NAMESPACE, ISSUER_NAME);
        expirationDuration = Integer.parseInt(configurator.getConfiguration(CONFIGURATION_NAMESPACE, EXPIRATION_DAY_LENGTH));

    }
}
