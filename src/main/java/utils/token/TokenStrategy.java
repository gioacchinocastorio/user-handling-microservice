package utils.token;

import business.pojo.User;

/**
 * Token generation generic algorithm
 */
public interface TokenStrategy {

    /** Generate the auth token for the specified user
     *
     * @param user owner of the token
     * @return generated token
     */
    String generateAuthToken(User user);

    /**
     * Check if the token is still valid (time based)
     * @param token token to check
     * @return true if expired
     */
    boolean isTokenExpired(String token);

    /**
     * Check if the token is valid
     * @param token auth token
     * @param userid ID of the token owner
     * @return true if valid
     */
    boolean isTokenValid(String token, String userid);

}
