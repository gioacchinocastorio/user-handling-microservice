package utils.config;

/**
 * Local configuration wrapper
 */
public interface Configurator {

    /**
     * Get a configuration value
     *
     * @param namespace configuration key namespace
     * @param confKey   key of the configuration in the conf_file
     * @return value as a string (to be casted)
     */
    String getConfiguration(String namespace, String confKey);
}
