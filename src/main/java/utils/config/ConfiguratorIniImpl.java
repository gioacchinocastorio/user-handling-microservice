package utils.config;

import org.ini4j.Ini;

import java.io.File;
import java.io.IOException;

/**
 * Ini file implementation of the configurator
 * See the interface for further details
 */
public class ConfiguratorIniImpl implements Configurator {

    private File configurationIniFile;
    private Ini configFile;

    /**
     * Constructor
     *
     * @param configFile file containing the configurations
     */
    public ConfiguratorIniImpl(File configFile) {
        this.configurationIniFile = configFile;
    }

    @Override
    public String getConfiguration(String namespace, String confKey) {
        this.fetchConfiguration(); // lazy load
        return this.configFile.get(namespace).fetch(confKey);
    }

    /**
     * Open the configuration JSON file and put it in the map
     */
    private void fetchConfiguration() {

        if (configFile == null) {
            try {

                // open the config file as an ini
                configFile = new Ini(configurationIniFile);

            } catch (IOException e) {
                System.err.println("INFO: cannot open the specified file");
                System.exit(1);
            }

        }

    }
}
